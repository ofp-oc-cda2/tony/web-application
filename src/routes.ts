import type { RouteRecordRaw } from "vue-router";
import { isAuthenticatedGuard, isNotAuthenticatedGuard } from "./shared/guards";
import Login from "./views/Login.vue";
import Register from "./views/Register.vue";
import Dashboard from "./views/Dashboard.vue";
import About from "./views/About.vue";
import Home from "./views/Home.vue";
import Product from "./views/Product.vue";
import EditProduct from "./views/EditProduct.vue";
import NotFound from "./views/NotFound.vue";


export const routes: RouteRecordRaw[] = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: "/login",
        name: 'Login',
        beforeEnter: [isNotAuthenticatedGuard],
        component: Login
    },
    {
        path: "/register",
        name: 'Register',
        beforeEnter: [isNotAuthenticatedGuard],
        component: Register
    },
    {
        path: "/about",
        name: 'About',
        beforeEnter: [isNotAuthenticatedGuard],
        component: About
    },
    {
        path: "/dashboard",
        name: 'Dashboard',
        beforeEnter: [isAuthenticatedGuard],
        component: Dashboard
    },
    {
        path: "/addproduct",
        name: 'Product',
        beforeEnter: [isAuthenticatedGuard],
        component: Product
    }
    ,
    {
        path: "/editproduct/:id",
        name: 'Edit',
        beforeEnter: [isAuthenticatedGuard],
        component: EditProduct
    }
    ,
    {
        path: '/:pathMatch(.*)*',
        name: 'not-found',
        component: NotFound
    }
    
]