import {test, expect} from 'vitest';
import {mount} from '@vue/test-utils';
import ProductForm from '../Form/ProductForm.vue';

test('Input quantity should be positive', () =>{
    const wrapper = mount(ProductForm, {});

    const input = wrapper.get(".quantity");
    expect(input.attributes('min="0"'))
    
})

test('Input limit_quantity should be positive', () =>{
    
})

test('Input expiration date have to be in present or futur', () =>{
    
})