import { createApp } from "vue";
import { createRouter, createWebHistory } from "vue-router";
import { createPinia } from "pinia";
import { routes } from "./routes";
import App from "./App.vue";
import "./assets/styles/index.scss";
import { useUser } from "./shared/stores";
// import logoutPlugin from "vue-auto-logout";

const router = createRouter({
  history: createWebHistory(),
  routes: routes,
});

router.beforeEach(() => {
  const userStore = useUser();
  if (!userStore.loaded) {
    userStore.fetchCurrentUser();
  }
});

const app = createApp(App);
// const logoutOptions = {
//   stagnateTime: 30 * 60 * 1000,
//   detectTime: 30 * 60,
// };
app.use(router);
app.use(createPinia());
// app.use(logoutPlugin, logoutOptions);
app.mount("#app");
