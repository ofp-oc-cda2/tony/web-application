import { defineStore } from "pinia";
import type { LoginForm, User } from "../interfaces/User.interface";
import { fetchCurrentUser, login } from "../services/auth.service";

interface UserState {
  currentUser: User | null;
  error: any;
  loaded: boolean;
  token: string | null;
}

export const useUser = defineStore("user", {
  state: (): UserState => ({
    currentUser: null,
    error: null,
    loaded: false,
    token: localStorage.getItem("token"),
  }),
  getters: {
    isAuthenticated(state): boolean {
      if (state.token) {
        return true;
      } else if (!state.token && state.loaded) {
        return false;
      } else {
        return false;
      }
    },
  },
  actions: {
    async login(loginForm: LoginForm) {
      try {
        console.log(loginForm);
        this.token = (await login(loginForm)).token;
        localStorage.setItem("token", this.token);
        this.error = null;
        this.fetchCurrentUser();
      } catch (e) {
        this.error = e;
        console.log("error login", e)
      }
    },
    async fetchCurrentUser() {
      try{
      this.currentUser = await fetchCurrentUser();
      this.loaded = true;
    }catch (e){
      this.error = e;
      console.log("error currentuser", e)
    }
    },
    async logout() {
      this.currentUser = null;
      this.loaded = false;
      this.token = null;
      localStorage.removeItem("token");
    },
  },
});
