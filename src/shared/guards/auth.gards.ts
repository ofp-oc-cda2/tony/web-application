import { useUser } from "../stores";

export function isAuthenticatedGuard() {
  const UserStore = useUser();
  if (!UserStore.isAuthenticated) {
    return "/login";
  }
}

export function isNotAuthenticatedGuard() {
  const UserStore = useUser();
  if (UserStore.isAuthenticated) {
    return "/";
  }
}
