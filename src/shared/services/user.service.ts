import type { UserForm } from "../interfaces";

const BASE_URL = import.meta.env.VITE_BASE_URL;

export function createUser(userForm: UserForm) {
  try {
    const response = fetch(BASE_URL + "users/register", {
      method: "POST",
      body: JSON.stringify(userForm),
      headers: {
        "Content-Type": "application/json",
      },
    });
    console.log(BASE_URL);
  } catch (e) {
    throw e;
  }
}
