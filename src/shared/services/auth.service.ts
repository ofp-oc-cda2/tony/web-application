import type { LoginForm, LoginResponse, User } from "../interfaces";
import { useUser } from "../stores";

const BASE_URL = import.meta.env.VITE_BASE_URL;

export async function login(loginForm: LoginForm): Promise<LoginResponse> {
  console.log(BASE_URL)
  const response = await fetch(BASE_URL + "users/login", {
    method: "POST",
    body: JSON.stringify(loginForm),
    headers: {
      "Content-Type": "application/json",
    },
  });
  if (response.ok) {
    return await response.json();
  } else {
    throw await response.json();
  }
}

export async function fetchCurrentUser(): Promise<User | null> {
  const UserStore = useUser();
  const response = await fetch(BASE_URL + "users/current", {
    headers: {
      authorization: `Bearer ${UserStore.token}`,
    },
  });
  return await response.json();
}
