import type { User } from "./User.interface";
import type { ExpirationAlert } from "./ExpirationAlert.interface";
import type { LimitQuantity } from "./LimitQuantity.interface";

export interface Product {
  id: number;
  name: string;
  quantity: number;
  type: string;
  UserId: number;
  User: User;
  ProductAlert: LimitQuantity;
  ExpirationAlert: ExpirationAlert | null;
}
