export interface User {
  id: number;
  email: string;
  password: string;
  premium: boolean;
  isAdmin: boolean;
}

export interface UserForm extends Partial<User> {}

export interface LoginForm {
  email: string;
  password: string;
}

export interface LoginResponse {
  id: number;
  token: string;
}
