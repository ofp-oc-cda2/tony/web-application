import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.helpyourmeal.app',
  appName: 'Help your Meal',
  webDir: 'dist',
  bundledWebRuntime: false
};

export default config;
